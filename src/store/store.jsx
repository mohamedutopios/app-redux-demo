import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import AddCartReducer from "../Reducers/AddCartReducer";
import CounterReducer from "../Reducers/CounterReducer";

const rootReducer = combineReducers({
    AddCartReducer,
    CounterReducer
})

const store = configureStore({reducer: rootReducer});

export default store;

