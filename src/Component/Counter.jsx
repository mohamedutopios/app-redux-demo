import { useState } from "react";
import { useDispatch, useSelector } from "react-redux"

export default function Counter() {

    const { cart, count } = useSelector(state => ({
        ...state.AddCartReducer,
        ...state.CounterReducer
    }))



    const [cartData, setCartData] = useState(0);

    const dispatch = useDispatch();


    const incrFunc = () => {
        dispatch({
            type: "INCR"
        })

    }


    const decrFunc = () => {
        dispatch({
            type: "DECR"
        })

    }

    const addCartDataFunc = () => {
        dispatch({
            type: "ADDCART",
            payload: cartData
        })


    }

    return (

        <div>
            <h1>Panier : {cart}  </h1>
            <h1>Comptage : {count}  </h1>
            <button onClick={incrFunc}> + 1 </button>
            <button onClick={decrFunc}> - 1 </button>
            <input value={cartData}
                onChange={e => setCartData(e.target.value)} type="number" />
            <br />
            <button onClick={addCartDataFunc}>Add to cart</button>

        </div>



    )




}